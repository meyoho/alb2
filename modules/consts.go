package modules

const (
	//ProtoHTTP is the protocol of http frontend
	ProtoHTTP  = "http"
	ProtoHTTPS = "https"
	ProtoTCP   = "tcp"
	ProtoUDP   = "udp"
)

const (
	TypeBind    = "bind"
	TypeIngress = "ingress"
)
