# Overview

Simple echo-server to test load-balancer, routing function. It will
- Show hostname the echo-server is running
- Show the version that defined by environment variable
- Show the headers send to echo-server
- Show the time the echo-server received message
- Show the data send to the echo-server

The echo server is based on Openresty, you can easily modify it to add new function.