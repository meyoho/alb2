user  nginx;
worker_processes     1;
daemon off;

error_log  /var/log/nginx/error.log warn;

env HOSTNAME;
env VERSION;

events {
    multi_accept        on;
    use                 epoll;
}


http {
    include       /usr/local/openresty/nginx/conf/mime.types;
    default_type  application/octet-stream;

    lua_code_cache off;
    lua_package_path '/usr/local/openresty/lualib/?.lua,;;';

    log_format  http  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"'
                      '"$gzip_ratio" $request_time $bytes_sent $request_length';
    access_log  /var/log/nginx/access.log  http buffer=16k flush=1s;

    sendfile        on;
    aio             threads;
    tcp_nopush      on;
    tcp_nodelay     on;
    log_subrequest  on;
    reset_timedout_connection on;

    proxy_set_header Host            $host;
    proxy_set_header X-Real-IP       $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_redirect             off;
    proxy_connect_timeout      5s;
    proxy_send_timeout         60s;
    proxy_read_timeout         60s;
    proxy_buffer_size          4k;
    proxy_buffers              4 32k;
    proxy_busy_buffers_size    64k;
    proxy_temp_file_write_size 64k;

    server {
        listen      8080 default_server;
        server_name _;

        location / {
            content_by_lua_file /lua/echoserver.lua;
        }
    }
}
